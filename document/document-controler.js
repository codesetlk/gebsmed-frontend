/**
 * Created by minindu on 11/5/16.
 */

$(document).ready(function() {
    $("#save").click(function() {
        var bucket=	configAws();

        uploadFile(bucket);

        // var JSONObject= {"name":companyName};

    });
    //get all products
    $.ajax({
        url : "http://localhost:8080/product/all",
        type : "GET",
        //This is what you should chage
        // dataType: "application/json",
        contentType : "application/json; charset=utf-8",
        user : sessionStorage.getItem("id"),
        // username: "user", // Most SAP web services require credentials
        //  password: "289f2619-bc1f-499f-89ed-7b349b0df6e5",
        // processData: false,
        success : function(data) {
            var productlist = data;
            setProductSelectOptions(productlist);
        },
        error : function(xhr, ajaxOptions, thrownError) { //Add these parameters to display the required response
            alert("Error");
        },
    });
    //get all stages
    $.ajax({
        url : "http://localhost:8080/docstage/all",
        type : "GET",
        //This is what you should chage
        // dataType: "application/json",
        contentType : "application/json; charset=utf-8",
        user : sessionStorage.getItem("id"),
        // username: "user", // Most SAP web services require credentials
        //  password: "289f2619-bc1f-499f-89ed-7b349b0df6e5",
        // processData: false,
        success : function(data) {
            var stagelist = data;
            setStageListSelectOptions(stagelist);
        },
        error : function(xhr, ajaxOptions, thrownError) { //Add these parameters to display the required response
            alert("Error");
        },
    });
    //get all uers
    $.ajax({
        url: "http://localhost:8080/user/all/",
        type: "GET",
        //This is what you should chage
        // dataType: "application/json",
        dataType: 'JSON',

        contentType: "application/json; charset=utf-8",
        async: false,
        user : sessionStorage.getItem("id"),
        // username: "user", // Most SAP web services require credentials
        //  password: "289f2619-bc1f-499f-89ed-7b349b0df6e5",
        // processData: false,
        success : function(data) {
            var userList = data;
            setUserListSelectOptions(userList);
        },
        error : function(xhr, ajaxOptions, thrownError) { //Add these parameters to display the required response
            alert("Error");
        },
    });


    //get all documents
    $.ajax({
        url : "http://localhost:8080/document/all",
        type : "GET",
        //This is what you should chage
        // dataType: "application/json",
        contentType : "application/json; charset=utf-8",
        user : sessionStorage.getItem("id"),
        // username: "user", // Most SAP web services require credentials
        //  password: "289f2619-bc1f-499f-89ed-7b349b0df6e5",
        // processData: false,
        success : function(data) {
            var docList = data;
            setDocListSelectOptions(docList);
        },
        error : function(xhr, ajaxOptions, thrownError) { //Add these parameters to display the required response
            alert("Error");
        },
    });
    //delete docment
    $("#delete").click(function() {
        var docId = $("#documentList option:selected").val();
        $.ajax({
            url : 'http://localhost:8080/document/delete/' + docId,
            type : "DELETE",
            //headers:{companyName},
            // data: JSON.stringify(datae),//This is what you should chage
            // dataType: "application/json",
            contentType : "application/json; charset=utf-8",
            user : sessionStorage.getItem("id"),
            // username: "user", // Most SAP web services require credentials
            //  password: "289f2619-bc1f-499f-89ed-7b349b0df6e5",
            // processData: false,
            success : function(data) {
                location.reload();
                //$("#productList").prop('selectedIndex', 0);

            },
            error : function(xhr, ajaxOptions, thrownError) { //Add these parameters to display the required response
                alert("Error");
            },
        });

    });
});

var setDocumentData = function(objKey) {
    var documentName = $("#docName").val();
    var stage = $("#stageList option:selected").val();
    var date = Date();
    //var file = $("#file").val(); aws sdk should used here
    var description = $("#descripton").val();
    var product = $("#productList option:selected").val();
    var user = $("#userList option:selected").val();

    var document = {
        "name" : documentName,
        "product" : {
            "id" : product
        },
        "date" : date,
        "user" : {
            "idUser" : user
        },
        "documentStage" : {
            "stageId" : stage
        },
        "objectKey":objKey,
    };

    return document;

};
//set product select options
var setProductSelectOptions = function(productlist) {
    if (productlist != null) {
        var optionsAsString = "";
        optionsAsString += "<option value=''></option>";

        for (var i = 0; i < productlist.length; i++) {
            optionsAsString += "<option value='" + productlist[i].id + "'>"
                + productlist[i].name + "</option>";
        }
        $("#productList").find('option').remove().end().append(
            $(optionsAsString));
    } else {
        var optionsAsString = "";
        optionsAsString += "<option value='NoTask'>" + 'No Tasks ToDO '
            + "</option>";
        $("#productList").find('option').remove().end().append(
            $(optionsAsString));

    }
};
var setStageListSelectOptions = function(stageList) {
    if (stageList != null) {
        var optionsAsString = "";
        optionsAsString += "<option value=''></option>";

        for (var i = 0; i < stageList.length; i++) {
            optionsAsString += "<option value='" + stageList[i].stageId + "'>"
                + stageList[i].name + "</option>";
        }
        $("#stageList").find('option').remove().end().append(
            $(optionsAsString));
    } else {
        var optionsAsString = "";
        optionsAsString += "<option value='NoTask'>" + 'No Tasks ToDO '
            + "</option>";
        $("#stageList").find('option').remove().end().append(
            $(optionsAsString));

    }
};
var setUserListSelectOptions = function(userlist) {
    if (userlist != null) {
        var optionsAsString = "";
        optionsAsString += "<option value=''></option>";

        for (var i = 0; i < userlist.length; i++) {
            optionsAsString += "<option value='" + userlist[i].idUser + "'>"
                + userlist[i].name + "</option>";
        }
        $("#userList").find('option').remove().end().append(
            $(optionsAsString));
    } else {
        var optionsAsString = "";
        optionsAsString += "<option value='NoTask'>" + 'No Tasks ToDO '
            + "</option>";
        $("#userList").find('option').remove().end().append(
            $(optionsAsString));

    }
};
var setDocListSelectOptions = function(doclist) {
    if (doclist != null) {
        var optionsAsString = "";
        optionsAsString += "<option value=''></option>";

        for (var i = 0; i < doclist.length; i++) {
            optionsAsString += "<option value='" + doclist[i].documentId + "'>"
                + doclist[i].name + "</option>";
        }
        $("#documentList").find('option').remove().end().append(
            $(optionsAsString));
    } else {
        var optionsAsString = "";
        optionsAsString += "<option value='NoTask'>" + 'No Tasks ToDO '
            + "</option>";
        $("#documentList").find('option').remove().end().append(
            $(optionsAsString));

    }
};
var configAws=function(){

    AWS.config.region = 'us-east-1'; //

    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId:'us-east-1:7fe98653-91d5-4f65-9a7b-8924055683ae'
    });

    AWS.config.credentials.get(function(err) {
        if (err) alert(err);
        console.log(AWS.config.credentials);
    });

    var bucketName = 'gebsmed'; // Enter your bucket name
    var bucket = new AWS.S3({
        params: {
            Bucket: bucketName
        }
    });
    return bucket;
};
var uploadFile=function(bucket){
    var fileChooser = document.getElementById('file-chooser');
    var file = fileChooser.files[0];
    var res="in";
    if (file) {
        var objKey = 'documents/' + file.name;
        var params = {
            Key: objKey,
            ContentType: file.type,
            Body: file,
            ACL: 'public-read'

        };

        bucket.upload(params, function(err, data) {
            if (err) {
                alert(err);

            } else {
                var document = setDocumentData(res);

                $.ajax({
                    url : "http://localhost:8080/document/create",
                    type : "POST",
                    data : JSON.stringify(document),//This is what you should chage
                    // dataType: "application/json",
                    contentType : "application/json; charset=utf-8",
                    user : sessionStorage.getItem("id"),
                    // username: "user", // Most SAP web services require credentials
                    //  password: "289f2619-bc1f-499f-89ed-7b349b0df6e5",
                    // processData: false,
                    success : function(data) {
                        location.reload();

                    },
                    error : function(xhr, ajaxOptions, thrownError) { //Add these parameters to display the required response
                        alert("Error");
                    },
                });
            }
        });
    }
    return objKey;
};