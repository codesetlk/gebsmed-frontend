/**
 * Created by minindu on 11/5/16.
 */

$(document).ready(function() {

    $.ajax({
        url : "http://localhost:8080/product/company/" + 3,
        type : "GET",
        contentType : "application/json; charset=utf-8",
        user : sessionStorage.getItem("id"),
        //This is what you should chage
        // dataType: "application/json",
//                    contentType : "application/json; charset=utf-8",

        // username: "user", // Most SAP web services require credentials
        //  password: "289f2619-bc1f-499f-89ed-7b349b0df6e5",
        // processData: false,
        success : function(data) {
            var productList = data;
            setProductList(productList);

            $(".btn-product").click(function () {
                var ProductId = $(this).data('id');

                $.ajax({
                    url : "http://localhost:8080/document/product/" + ProductId,
                    type : "GET",
                    contentType : "application/json; charset=utf-8",
                    user : sessionStorage.getItem("id"),
//                            user : sessionStorage.getItem("id"),
                    //This is what you should chage
                    // dataType: "application/json",
//                                contentType : "application/json; charset=utf-8",

                    // username: "user", // Most SAP web services require credentials
                    //  password: "289f2619-bc1f-499f-89ed-7b349b0df6e5",
                    // processData: false,
                    success : function(data) {
                        var docList = data;
                        setDocumentList(docList);
                    },
                    error : function(xhr, ajaxOptions, thrownError) { //Add these parameters to display the required response
                        alert("Error");
                    },
                });

            });

        },
        error : function(xhr, ajaxOptions, thrownError) { //Add these parameters to display the required response
            alert("Error");
        },
    });
});

var setProductList = function(productList) {
    if (productList.length != 0) {
        var optionsAsString = "";
        for (var i = 0; i < productList.length; i++) {
            optionsAsString += "<tr><td>"
                + "<button style='background-color: transparent;' class='btn btn-block btn-product' data-id='" + productList[i].id + "'>" + productList[i].name + "</button>"
                + "</td></tr>";

            $("#productTable").find('tbody').remove().end().append(
                $(optionsAsString));
        }
    } else {
        var optionsAsString = "";
        optionsAsString += "<tr><td>No Products</td></tr>";
        $("#productTable").find('tbody').remove().end().append(
            $(optionsAsString));

    }
};

var setDocumentList = function (docList) {

    var simpleTableData = "<tr>";
    var processingTableData = "<tr>";
    var primaryTableData = "<tr>";
    var fullTableData = "<tr>";

    if (docList.length != 0) {
        for (var i=0; i < docList.length; i++) {
            var documentStage = docList[i].documentStage;
            switch (documentStage.type) {
                case 1 :
                    simpleTableData += "<td><p>" + docList[i].name + "</p></td>";
                    break;
                case 2 :
                    processingTableData += "<td><p>" + docList[i].name + "</p></td>";
                    break;
                case 3 :
                    primaryTableData += "<td><p>" + docList[i].name + "</p></td>";
                    break;
                case 4 :
                    fullTableData += "<td><p>" + docList[i].name + "</p></td>";
                    break;
            }
        }

        simpleTableData += "</tr>";
        processingTableData += "</tr>";
        primaryTableData += "</tr>";
        fullTableData += "</tr>";

        $("#sampleTable").find('tbody').remove().end().append($(simpleTableData));
        $("#processingTable").find('tbody').remove().end().append($(processingTableData));
        $("#primaryTable").find('tbody').remove().end().append($(primaryTableData));
        $("#fullTable").find('tbody').remove().end().append($(fullTableData));

    } else {

        $("#sampleTable").find('tbody').remove();
        $("#processingTable").find('tbody').remove();
        $("#primaryTable").find('tbody').remove();
        $("#fullTable").find('tbody').remove();

    }
};